# Evaluating domain-specific vs. general Transformer models for question answering on expert and non-expert questions about COVID-19

This research project is a small-scale study in the area of question-answering (QA) in machine learning (ML). The main aim is to investigate whether the level of domain knowledge of a speaker who asks a question also impacts answer accuracy.
To this end the pre-trained Transformer models BERT-base (Devlin et al. (2019) and BioBERT (Lee et al. (2020) are used to generate answers to questions of biomedical experts and non-experts about the current COVID-19 disease.

Two question sets as well as the document collection for answer extraction were provided as part of the 'Epidemic Question Answering' (EPIC) track of the ‘Text Analysis Conference’ and can be found in the directory 'supplementary_material'.

All scripts and Jupyter Notebooks for data preprocessing and answer extraction are stored under 'data'.
Material and code related to evaluation and data analysis as well as visualisation is provided in the folder 'evaluation'.

For the results of question and answer analysis see the resources in 'questions' and 'answers' respectively.
